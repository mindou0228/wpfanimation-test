﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFAnimationTest
{
    public class UserInfo : INotifyPropertyChanged
    {
        private int _age;

        public int Age
        {
            get => _age;
            set
            {
                _age = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Age"));
                }
            }
        }

        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }

        private string _sex;

        public string Sex
        {
            get => _sex;
            set
            {
                _sex = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Sex"));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}