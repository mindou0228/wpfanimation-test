﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFAnimationTest
{
    /// <summary>
    /// TriggerDemo.xaml 的交互逻辑
    /// </summary>
    public partial class TriggerDemo : UserControl, INotifyPropertyChanged
    {
        private ObservableCollection<UserInfo> _userInfos;

        public ObservableCollection<UserInfo> UserInfos
        {
            get => _userInfos;
            set
            {
                _userInfos = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("UserInfos"));
                }
            }
        }

        public TriggerDemo()
        {
            InitializeComponent();
            UserInfos = new ObservableCollection<UserInfo>();
            UserInfos.Add(new UserInfo
            {
                Age = 10,
                Name = "张三",
                Sex = "男"
            });
            UserInfos.Add(new UserInfo
            {
                Age = 20,
                Name = "李四",
                Sex = "男"
            });
            UserInfos.Add(new UserInfo
            {
                Age = 30,
                Name = "王二麻子",
                Sex = "女"
            });
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}